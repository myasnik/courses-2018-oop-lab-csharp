using System;
using System.Collections.Generic;
using System.Linq;

namespace Iterators {

    public static class Java8StreamOperations
    {
        public static void ForEach<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            var enumerator = sequence.GetEnumerator();
            do
            {
                consumer.Invoke(enumerator.Current);
            }
            while (enumerator.MoveNext());
        }

        public static IEnumerable<TAny> Peek<TAny>(this IEnumerable<TAny> sequence, Action<TAny> consumer)
        {
            var enumerator = sequence.GetEnumerator();
            enumerator.MoveNext();
            do
            {
                var e = enumerator.Current;
                consumer.Invoke(e);
                yield return e;
            }
            while (enumerator.MoveNext());
        }

        public static IEnumerable<TOther> Map<TAny, TOther>(this IEnumerable<TAny> sequence, Func<TAny, TOther> mapper)
        {
            var enumerator = sequence.GetEnumerator();
            do
            {
                yield return mapper.Invoke(enumerator.Current);
            }
            while (enumerator.MoveNext());
        }

        public static IEnumerable<TAny> Filter<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            var enumerator = sequence.GetEnumerator();
            do
            {
                if (consumer.Invoke(enumerator.Current))
                    yield return enumerator.Current;
            }
            while (enumerator.MoveNext());
        }

        public static IEnumerable<Tuple<int, TAny>> Indexed<TAny>(this IEnumerable<TAny> sequence)
        {
            int i = 0;
            var enumerator = sequence.GetEnumerator();
            do
            {
                yield return new Tuple<int, TAny>(i, enumerator.Current);
            }
            while (enumerator.MoveNext());
        }

        public static TOther Reduce<TAny, TOther>(this IEnumerable<TAny> sequence, TOther seed, Func<TOther, TAny, TOther> reducer)
        {
            var enumerator = sequence.GetEnumerator();
            TOther res;
            enumerator.MoveNext();
            do
            {
                res = reducer.Invoke(seed, enumerator.Current);
            }
            while (enumerator.MoveNext());
            return res;
        }

        public static IEnumerable<TAny> SkipWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            var enumerator = sequence.GetEnumerator();
            while (consumer.Invoke(enumerator.Current))
            { 
                enumerator.MoveNext();
            }
            do
            {
                yield return enumerator.Current;
            }
            while (enumerator.MoveNext());
        }

        public static IEnumerable<TAny> SkipSome<TAny>(this IEnumerable<TAny> sequence, long count)
        { 
            var enumerator = sequence.GetEnumerator();
            for (long i = 0; i <= count; i++)
            {
                enumerator.MoveNext();
            }
            do
            {
                yield return enumerator.Current;
            }
            while (enumerator.MoveNext());
        }

        public static IEnumerable<TAny> TakeWhile<TAny>(this IEnumerable<TAny> sequence, Predicate<TAny> consumer)
        {
            var enumerator = sequence.GetEnumerator();
            while (consumer.Invoke(enumerator.Current))
            {
                yield return enumerator.Current;
                enumerator.MoveNext();
            }
        }

        public static IEnumerable<TAny> TakeSome<TAny>(this IEnumerable<TAny> sequence, long count)
        {
            long i = 0;
            var enumerator = sequence.GetEnumerator();
            do
            {
                yield return enumerator.Current;
                i++;
            }
            while (enumerator.MoveNext() && i < count);
        }

        public static IEnumerable<int> Integers(int start)
        {
            while(true)
            {
                yield return start;
                start = start + 1;
            }
        }

        public static IEnumerable<int> Integers()
        {
            return Integers(0);
        }

        public static IEnumerable<int> Range(int start, int count)
        {
            return Integers().TakeSome(count);
        }
    }

}