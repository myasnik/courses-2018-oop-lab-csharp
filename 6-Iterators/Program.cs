﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterators
{

    class Program
    {

        static void Main(string[] args)
        {
            const int len = 50;
            int?[] numbers = new int?[len];
            Random rand = new Random();
            for (int i = 0; i < len; i++)
            {
                if (rand.NextDouble() > 0.2)
                {
                    numbers[i] = rand.Next(len);
                }
            }

            // TODO rewrite using methods from Java8StreamOperations
            IDictionary<int, int> occurrences = numbers.Select(optN => {
                    Console.Write(optN.ToString() + ",");
                    return optN;
                }).Skip(1)
                .Take(len - 2)
                .Where(optN => optN.HasValue)
                .Select(optN => optN.Value)
                .Aggregate(new Dictionary<int, int>(), (d, n) => {
                    if (!d.ContainsKey(n))
                    {
                        d[n] = 1;
                    }
                    else
                    {
                        d[n]++;
                    }
                    return d;
                });

            Console.WriteLine();

            foreach (KeyValuePair<int, int> kv in occurrences)
            {
                Console.WriteLine(kv);
            }

            //Java

            IDictionary<int, int> occurrences1 = numbers
                .SkipSome(1)
                .Peek(optN =>
                {
                    Console.Write(optN.ToString() + ",");
                })
                .TakeSome(len - 2)
                .Filter(optN => optN.HasValue)
                .Select(optN => optN.Value)
                .Reduce(new Dictionary<int, int>(), (d, n) => {
                    if (!d.ContainsKey(n))
                    {
                        d[n] = 1;
                    }
                    else
                    {
                        d[n]++;
                    }
                    return d;
                });

            Console.WriteLine();

            foreach (KeyValuePair<int, int> kv in occurrences1)
            {
                Console.WriteLine(kv);
            }

            Console.ReadLine();
        }
    }
}
