﻿using System;

namespace ExtensionMethods
{
    internal class Complex : IComplex
    {
        private double re;
        private double im;

        public Complex(double re, double im)
        {
            this.re = re;
            this.im = im;
        }

        public bool Equals(IComplex other)
        {
            if (re == other.Real && im == other.Imaginary) return true;
            else return false;
        }

        public double Real
        {
            get
            {
                return re;
            }
        }

        public double Imaginary
        {
            get
            {
                return im;
            }
        }
        public double Modulus
        {
            get
            {
                return Math.Sqrt((re * re) + (im * im));
            }
        }
        public double Phase
        {
            get
            {
                if (re == 0 && im > 0) return Math.PI / 2;
                if (re == 0 && im < 0) return -Math.PI / 2;
                if (re == 0 && im == 0) return 0;
                if (re > 0) return Math.Atan(im / re);
                if (re < 0 && im >= 0) return Math.Atan(im / re) + Math.PI;
                if (re < 0 && im < 0) return Math.Atan(im / re) - Math.PI;
                throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            if(im >= 0) return "z = " + re + " +" + im + "i";
            else return "z = " + re + " " + im + "i";
        }

        public override bool Equals(object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            var hashCode = -196947237;
            hashCode = hashCode * -1521134295 + re.GetHashCode();
            hashCode = hashCode * -1521134295 + im.GetHashCode();
            return hashCode;
        }
    }
}