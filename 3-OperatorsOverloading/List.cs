﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorsOverloading
{
    public abstract class List<TValue> 
    {
        internal sealed class HeadTail<THead> : List<THead>
        {
            public override THead Head { get; }
            public override List<THead> Tail { get; }

            public HeadTail(THead head, List<THead> tail)
            {
                this.Head = head;
                this.Tail = tail ?? new List<THead>.Empty<THead>();
            }

            public override bool IsNil
            {
                get { return false; }
            }

            public override int Length
            {
                get { return 1 + this.Tail.Length; }
            }
        }

        internal sealed class Empty<TList> : List<TList>
        {
            public override TList Head { get { return default(TList); } }
            public override List<TList> Tail { get { return null; } }
            public override bool IsNil { get { return true; } }
            public override int Length
            {
                get { return 0; }
            }
        }

        public abstract TValue Head { get; }

        public abstract List<TValue> Tail { get; }

        public abstract bool IsNil { get; }

        public abstract int Length { get; }

        public IEnumerable<List<TValue>> Flatten()
        {
            List<TValue> curr = this;
            for (; !curr.IsNil; curr = curr.Tail)
            {
                yield return curr;
            }
        }

        public override string ToString()
        {
            return "[ " + string.Join(", ", this.Flatten()
                       .Select(l => l.Head)) +" ]";
        }

        private static bool equ(List<TValue> list1, List<TValue> list2)
        {
            if (!list1.Head.Equals(list2.Head)) return false;
            else return equ(list1.Tail, list2.Tail);
        }

        public static bool operator ==(List<TValue> list1, List<TValue> list2)
        {
            return equ(list1, list2);
        }

        private static bool div(List<TValue> list1, List<TValue> list2)
        {
            if (list1.Head.Equals(list2.Head)) return false;
            else return div(list1.Tail, list2.Tail);
        }

        public static bool operator !=(List<TValue> list1, List<TValue> list2)
        {
            return div(list1, list2);
        }

        public static bool operator >=(List<TValue> list1, List<TValue> list2)
        {
            return list1.Length >= list2.Length;
        }

        public static bool operator <=(List<TValue> list1, List<TValue> list2)
        {
            return list1.Length <= list2.Length;
        }

        public static bool operator <(List<TValue> list1, List<TValue> list2)
        {
            return list1.Length < list2.Length;
        }

        public static bool operator >(List<TValue> list1, List<TValue> list2)
        {
            return list1.Length > list2.Length;
        }

        public static List<TValue> operator +(List<TValue> list1, List<TValue> list2)
        {
            return List.Append(list1, list2);
        }

        private static List<TValue> del(List<TValue> newlist, List<TValue> list, TValue elem)
        {
            if (list.Head.Equals(elem))
            {
                if (list.Tail.IsNil) return newlist;
                return del(newlist, list.Tail, elem);
            }
            else
            {
                if (list.Tail.IsNil) return newlist;
                return del(List.Append(newlist, list.Head), list.Tail, elem);
            }
        }

        private static List<TValue> min(List<TValue> list1, List<TValue> list2)
        {
            if (list1.IsNil || list2.IsNil) return list1;
            if (list1.Head.Equals(list2.Head)) return min(list1.Tail, list2.Tail);
            List<TValue> list = List.Of(list1.Head);
            return min(del(list, list1.Tail, list2.Head), list2.Tail);
        }

        public static List<TValue> operator -(List<TValue> list1, List<TValue> list2)
        {
            return min(list1, list2);
        }

        public static implicit operator List<TValue>(TValue[] enumerable)
        {
            if (enumerable.Length == 0) return List.Nil<TValue>();
            if (enumerable.Length == 1) return List.Of(enumerable[0]);
            List<TValue> list = List.Of(enumerable[0]);
            for (int i = 1; i < enumerable.Length; i++)
            {
                list = List.Append(list, enumerable[i]);
            }
            return list;
        }

        public static implicit operator List<TValue>(TValue element)
        {
            return List.Of(element);
        }

        private static TValue[] castlisttoarray(TValue[] arr, List<TValue> list, int i)
        {
            if (list.IsNil) return arr;
            else
            {
                arr[i] = list.Head;
                return castlisttoarray(arr, list.Tail, i - 1);
            }
        }

        public static explicit operator TValue[] (List<TValue> enumerable)
        {
            int i = enumerable.Length - 1;
            TValue[] arr = new TValue[enumerable.Length];
            return castlisttoarray(arr, enumerable, i);
        }
    }

    public static class List
    {
        public static List<TItem> Of<TItem>(TItem head)
        {
            return List.Of(head, Nil<TItem>());
        }

        public static List<TItem> Of<TItem>(TItem head, List<TItem> tail)
        {
            return new List<TItem>.HeadTail<TItem>(head, tail);
        }

        public static List<TItem> Nil<TItem>()
        {
            return new List<TItem>.Empty<TItem>();
        }

        public static List<TItem> From<TItem>(IEnumerable<TItem> items)
        {
            var elems = items.Reverse().ToArray();
            List<TItem> curr = List.Of(elems.First());

            foreach (var e in elems.Skip(1))
            {
                curr = List.Of(e, curr);
            }

            return curr;
        }

        public static List<TItem> From<TItem>(TItem item1, params TItem[] items)
        {
            return List.From(Enumerable.Repeat(item1, 1).Concat(items));
        }

        public static List<TItem> Append<TItem>(List<TItem> list1, List<TItem> list2)
        {
            var elems = list1.Flatten().Select(l => l.Head).Reverse().ToArray();
            List<TItem> curr = List.Of(elems.First(), list2);

            foreach (var e in elems.Skip(1))
            {
                curr = List.Of(e, curr);
            }

            return curr;
        }
    }


    

}
