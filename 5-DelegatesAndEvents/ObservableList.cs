﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DelegatesAndEvents {

    public class ObservableList<TItem> : IObservableList<TItem>
    {
        List<TItem> list = new List<TItem>();

        public IEnumerator<TItem> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public void Add(TItem item)
        {
            list.Add(item);
            ElementInserted?.Invoke(this, item, this.IndexOf(item));
        }

        public void Clear()
        {
            foreach (TItem elem in this)
            {
                Remove(elem);
            }
        }

        public bool Contains(TItem item)
        {
            return list.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(TItem item)
        {
            ElementRemoved?.Invoke(this, item, this.IndexOf(item));
            return list.Remove(item);
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return IsReadOnly;
            }
        }

        public int IndexOf(TItem item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            TItem item1 = list[index]; 
            list.Insert(index, item);
            ElementChanged?.Invoke(this, item, item1, index);
        }

        public void RemoveAt(int index)
        {
            TItem item = list[index];
            list.Remove(item);
            ElementRemoved?.Invoke(this, item, index);
        }

        public TItem this[int index]
        {
            get { return list[index]; }
            set
            {
                TItem item = list[index];
                list.Insert(index, value);
                ElementChanged?.Invoke(this, value, item, index);
            }
        }

        public event ListChangeCallback<TItem> ElementInserted;
        public event ListChangeCallback<TItem> ElementRemoved;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }

        public override int GetHashCode()
        {
            var hashCode = -259807572;
            hashCode = hashCode * -1521134295 + EqualityComparer<List<TItem>>.Default.GetHashCode(list);
            hashCode = hashCode * -1521134295 + Count.GetHashCode();
            hashCode = hashCode * -1521134295 + IsReadOnly.GetHashCode();
            return hashCode;
        }
    }

}