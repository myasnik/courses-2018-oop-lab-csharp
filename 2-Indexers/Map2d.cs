﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        Dictionary<Tuple<TKey1, TKey2>, TValue> dict = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            return dict.Equals(other);
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get
            {
                return dict[new Tuple<TKey1, TKey2>(key1, key2)];
            }
            set
            {
                Tuple<TKey1, TKey2> tuple = new Tuple<TKey1, TKey2>(key1,key2);
                dict.Add(tuple, value);
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {/*
            List<Tuple<TKey2, TValue>> resList = new List<Tuple<TKey2, TValue>>();
            foreach (KeyValuePair<Tuple<TKey1, TKey2>, TValue> elem in dict)
            {
                if (elem.Key.Item1.Equals(key1))
                {
                    resList.Add(new Tuple<TKey2, TValue>(elem.Key.Item2, elem.Value));
                }
            }
            return resList;*/
            return GetFromMatrix((k1, k2) => k1.Equals(key1), (k1, k2) => k2);
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {/*
            List<Tuple<TKey1, TValue>> resList = new List<Tuple<TKey1, TValue>>();
            foreach (KeyValuePair<Tuple<TKey1, TKey2>, TValue> elem in dict)
            {
                if (elem.Key.Item2.Equals(key2))
                {
                    resList.Add(new Tuple<TKey1, TValue>(elem.Key.Item1, elem.Value));
                }
            }
            return resList;*/
            return GetFromMatrix((k1, k2) => k2.Equals(key2), (k1, k2) => k1);
        }
        public IList<Tuple<TKey, TValue>> GetFromMatrix<TKey>(Func<TKey1, TKey2, bool> shouldAddLine, Func<TKey1, TKey2, TKey> pick)
        {
            List<Tuple<TKey, TValue>> resList = new List<Tuple<TKey, TValue>>();
            foreach (KeyValuePair<Tuple<TKey1, TKey2>, TValue> elem in dict)
            {
                if (shouldAddLine(elem.Key.Item1, elem.Key.Item2))
                {
                    resList.Add(new Tuple<TKey, TValue>(pick(elem.Key.Item1, elem.Key.Item2), elem.Value));
                }
            }
            return resList;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            List<Tuple<TKey1, TKey2>> keyList = new List<Tuple<TKey1, TKey2>>(dict.Keys);
            List<Tuple<TKey1, TKey2, TValue>> resList = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (Tuple<TKey1, TKey2> elem in keyList)
            {
                Tuple<TKey1, TKey2, TValue> tupla = new Tuple<TKey1, TKey2, TValue>(elem.Item1, elem.Item2, dict[elem]);
                resList.Add(tupla);
            }
            return resList;
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            if (keys1.Count() != keys2.Count()) return;
            for(int i = 0; i < keys1.Count(); i++)
            {
                for (int j = 0; j < keys2.Count(); j++)
                {
                    Tuple<TKey1, TKey2> tupla = new Tuple<TKey1, TKey2>(keys1.ElementAt(i), keys2.ElementAt(j));
                    dict.Add(tupla, generator.Invoke(keys1.ElementAt(i), keys2.ElementAt(j)));
                }
            }
        }

        public int NumberOfElements
        {
            get
            {
                return dict.Count();
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
